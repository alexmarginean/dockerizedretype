#!/bin/bash

CurrentPath=$(pwd)
echo < $CurrentPath

#create the shared folder
mkdir retypeDockerSharedFolder
#git clone https://alexmarginean@bitbucket.org/alexmarginean/typechange.git

sudo docker build -t development/rose .
#sudo docker run -P -v retypeDockerSharedFolder:/home/development alexmarginean/rose /bin/sh -c 'cd /home/development; git clone https://alexmarginean@bitbucket.org/alexmarginean/typechange.git'

sudo docker run -P -e --rm -v $CurrentPath/retypeDockerSharedFolder:/root development/rose /bin/sh -c 'mkdir /root/development; cd /root/development; git clone https://alexmarginean@bitbucket.org/alexmarginean/typechange.git; cd /root/development/typechange; autoreconf -i; ./configure CPPFLAGS="-I/home/ROSE/RoseInstallTree/include/rose -I/home/ROSE/BoostInstallTree/include" LDFLAGS="-L/home/ROSE/RoseInstallTree/lib -L/home/ROSE/BoostInstallTree/lib -L/usr/lib/jdk1.8.0_25/jre/lib/amd64/server" CFLAGS="-O0 -g" CXXFLAGS="-O0 -g"; make; make check '

#sudo docker run -P -e --rm -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix -v $CurrentPath/retypeDockerSharedFolder:/root development/rose /bin/sh -c 'eclipse'




#./configure CPPFLAGS="-I/home/ROSE/RoseInstallTree/include/rose -I/home/ROSE/BoostInstallTree/include" LDFLAGS="-L/home/ROSE/RoseInstallTree/lib -L/home/ROSE/BoostInstallTree/lib -L/usr/lib/jdk1.8.0_25/jre/lib/amd64/server" CFLAGS="-O0 -g" CXXFLAGS="-O0 -g"

