Instalation instruction:

* [Install]
* * Run ./dockerizeRetype.sh. This may take a while, because we have to build ROSE here. After it you will have a full instalation of ROSE compiler infrastructure. The folder retypeDockerSharedFolder is the folder shared between the filesystem and Docker. Here we will clone the "retype" repository. Also the workspace from the docker image will be here. This is needed for persisting configurations of eclipse and existing projects between sessions.

* [Run]
* * For developing / testing retype you have to run the script ./run.sh . If you want to run just eclipse, you can run: ./run.sh eclipse . If you run: ./run.sh , then you will enter in an interactive shell. 
From here you can also start eclipse, by running: eclipse . The project is already configured and tested. The path to the project is: /root/development/typechanger . 


* First time usage of eclipse: 
	Once entered in eclipse you can import the project: File -> New -> Makefile Project with Existing Code and follow the wizzard; after this is finished: File -> New -> Convert to a C/C++ Autotools Project . After the project is configured right click on the project; Properties; expand Autotools; Configure Settings; expand configure; Advanced; copy and paste in the box near Additional command-line options:
		CPPFLAGS="-I/home/alex/Development/Tools/RoseEDG_4_x/InstallTree/include -I/home/alex/Development/Tools/RoseEDG_4_x/InstallTree/include/rose -I/home/alex/Development/Tools/Boost/Boost_1_45_install/include" LDFLAGS="-L/home/alex/Development/Tools/RoseEDG_4_x/InstallTree/lib -L/home/alex/Development/Tools/Boost/Boost_1_45_install/lib -L/usr/lib/jvm/jdk1.8.0_25/jre/lib/amd64/server" CFLAGS="-O0 -g" CXXFLAGS="-O0 -g" . Run Project -> Build All, and then you will be able to use eclipse for retype.

	After the first configuration the settings will be persistent in retypeDockerSharedFolder . 

** The current scripts and configuration should work on any Linux machine. The image by itself may be used on Windows / Mac OS X. However you must assure that the scripts run from the VM in which docker runs.